package Rozetka_Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RecycleBinPage {
    private WebDriver driver;
    public RecycleBinPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    //CheckEmptyRecycleBin test
    @FindBy(xpath = ".//*[@class='empty-cart-title inline sprite-side']")
    public WebElement Empty_Recycle_Bin;
}



