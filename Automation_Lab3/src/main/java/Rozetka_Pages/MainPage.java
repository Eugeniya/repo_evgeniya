package Rozetka_Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class MainPage {
    public WebDriver driver;

    public MainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    //CheckLogoDisplay test
    @FindBy(xpath = ".//*[@class='logo']/img")
    public WebElement Image_Logo;

    //CheckCatalogMenuContainsAppleItem test
    @FindBy(xpath = ".//*[@id='m-main']/li/a[contains(text(),'Apple')]")
    public WebElement Apple_Item;

    //CheckCatalogMenuContainsMP3Item test
    @FindBy(xpath = ".//*[@id='m-main']/li/a[contains(text(),'MP3')]")
    public WebElement MP3_Item;

    //CheckCitySelectionInCitySection test
    @FindBy(xpath = ".//*[@id='city-chooser']")
    public WebElement City_Chooser;
    @FindBy(xpath = ".//*[@class='header-city-i']/*[contains(text(),'Харьков')]")
    public WebElement City_Kharkov;
    @FindBy(xpath = ".//*[@class='header-city-i']/*[contains(text(),'Киев')]")
    public WebElement City_Kyiv;
    @FindBy(xpath = ".//*[@class='header-city-i']/*[contains(text(),'Одесса')]")
    public WebElement City_Odessa;

    public void ClickCityButton() throws InterruptedException {

        City_Chooser.click();

    }

    //Opening Recycle Bin
    @FindBy(xpath = ".//*[@class='sprite-side novisited hub-i-link hub-i-cart-link']")
    public WebElement Recycle_Bin;
    public RecycleBinPage RecycleBinButton() throws InterruptedException {
        Recycle_Bin.click();
        return new RecycleBinPage(driver);
    }

}
