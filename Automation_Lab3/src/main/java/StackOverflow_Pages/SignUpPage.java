package StackOverflow_Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignUpPage {
    public WebDriver driver;

    public SignUpPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[@class='text']/span[contains(text(),'Google')]")
    public static WebElement Google_Button;
    @FindBy(xpath = ".//*[@class='text']/span[contains(text(),'Facebook')]")
    public static WebElement Facebook_Button;
}
