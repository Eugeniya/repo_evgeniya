package StackOverflow_Pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class StartPage {
    public WebDriver driver;

    public StartPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    //checkfeaturedNumberMoreThan300 test
    @FindBy(xpath = ".//*[@class='bounty-indicator-tab']")
    public static WebElement Featured_Number_Icon;
        public void checkfeaturedNumber() throws Exception {
        String number= StartPage.Featured_Number_Icon.getText();
        int count=Integer.parseInt(number);
        assertTrue("Count is not > 300", count>300);
    }


    //checkSignUpFromGoogleAndFacebook test
    @FindBy(xpath = "//*[@class='topbar-menu-links']/a[contains (text(), 'sign up')]")
    public static WebElement Sign_Up;
    public SignUpPage signUpPage () throws InterruptedException {
        Sign_Up.click();
        return new SignUpPage(driver);

    }

    //checkTopQuestionAskedToday test
    @FindBy(xpath = ".//*[@id='qinfo']//b[text()='today']")
    public static WebElement Asked_Question;

    public void AskedQuestionToday() throws InterruptedException {
        int questionIndex = (int) (Math.random() * 10) + 1;
        String xPathForQuestion = ".//*[@id='question-mini-list']/div[" + questionIndex + "]//a[@class='question-hyperlink']";
        driver.findElement(By.xpath(xPathForQuestion)).click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

}
