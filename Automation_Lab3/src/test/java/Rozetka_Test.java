import Rozetka_Pages.MainPage;
import Rozetka_Pages.RecycleBinPage;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.*;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import java.util.concurrent.TimeUnit;
import java.util.List;

public class Rozetka_Test {
    private WebDriver driver;
    private String baseUrl;
    protected MainPage mainPage;
    protected RecycleBinPage recycleBinPage;

    @Before
    public void beforeSetUp() throws Exception{
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        baseUrl = "http://rozetka.com.ua";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(baseUrl);
        mainPage = new MainPage(driver);
    }

    @After
    public void after() throws Exception{
        driver.close();
    }

    @Test
    public void CheckLogoDisplay() throws InterruptedException {
        Assert.assertTrue("logo is not shown", mainPage.Image_Logo.isDisplayed());
    }

    @Test
    public void CheckCatalogMenuContainsAppleItem() throws InterruptedException {
        Assert.assertTrue("Menu does not contain the 'Apple' item", mainPage.Apple_Item.isDisplayed());
    }

    @Test
    public void CheckCatalogMenuContainsMP3Item() throws InterruptedException {
        Assert.assertTrue("There is no item containing 'MP3' in menu", mainPage.MP3_Item.isDisplayed());

    }
    @Test
    public void CheckCitySelectionInCitySection() throws InterruptedException {
        mainPage.ClickCityButton();
        Assert.assertTrue("Город' is not displayed", mainPage.City_Chooser.isDisplayed());

        Assert.assertTrue("city 'Харьков' is not displayed", mainPage.City_Kharkov.isDisplayed());
        Assert.assertTrue("city 'Киев' is not displayed", mainPage.City_Kyiv.isDisplayed());
        Assert.assertTrue("city 'Одесса' is not displayed", mainPage.City_Odessa.isDisplayed());
    }
    @Test
    public void CheckEmptyRecycleBin() throws InterruptedException {
        //RecycleBinPage = mainPage.Recycle_Bin.click();
        Assert.assertTrue("Message'Корзина пуста' is not shown", mainPage.Recycle_Bin.isDisplayed());
    }
}