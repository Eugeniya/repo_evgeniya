import StackOverflow_Pages.SignUpPage;
import StackOverflow_Pages.StartPage;
import com.sun.org.apache.xpath.internal.operations.Number;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.*;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import java.io.File;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.assertTrue;

public class StackOverflow_Test {
    private WebDriver driver;
    private String baseUrl;
    protected StartPage startPage;
    protected SignUpPage signUpPage;


    @Before
    public void beforeSetUp() throws Exception{
        driver = new FirefoxDriver();
        baseUrl = "http://stackoverflow.com/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(baseUrl);
        startPage = new StartPage(driver);
        signUpPage = new SignUpPage(driver);
    }

    @After
    public void afterSetUp() throws Exception {
        driver.close();
    }

@Test
    public void checkfeaturedNumberMoreThan300() throws Exception {
        startPage.checkfeaturedNumber();
    }

    @Test
    public void checkSignUpFromGoogleAndFacebook() throws InterruptedException {

        startPage.Sign_Up.click();
        Assert.assertTrue("Google button is missing", SignUpPage.Google_Button.isDisplayed());
        Assert.assertTrue("Facebook button is missing", SignUpPage.Facebook_Button.isDisplayed());
    }

    @Test
    public void checkTopQuestionAskedToday() throws Exception {
        startPage.AskedQuestionToday();

        Assert.assertTrue("Question was not asked today", StartPage.Asked_Question.isDisplayed());
    }
}