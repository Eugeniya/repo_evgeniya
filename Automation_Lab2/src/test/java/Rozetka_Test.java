import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import java.util.concurrent.TimeUnit;
import java.util.List;

public class Rozetka_Test {
    private WebDriver driver;
    private String baseUrl;

    @BeforeClass
    public static void beforeClassSetUp() throws Exception{

    }

    @Before
    public void beforeSetUp() throws Exception{
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        baseUrl = "http://rozetka.com.ua";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(baseUrl);
    }

    @After
    public void after() throws Exception{
        driver.quit();
    }

    @AfterClass
    public static void afterClass() throws Exception{
    }

    @Test
    public void testLogo() throws InterruptedException {
        Assert.assertTrue("logo is not shown",
                driver.findElement(By.xpath(".//*[@class='logo']/img")).isDisplayed());
    }

    @Test
    public void testApple() throws InterruptedException {
        Assert.assertTrue("Menu does not contain the 'Apple' item",
                driver.findElement(By.xpath(".//*[@id='m-main']/li/a[contains(text(),'Apple')]")).isDisplayed());
    }

    @Test
    public void testMP3() throws InterruptedException {
        Assert.assertTrue("There is no item containing 'MP3' in menu",
                driver.findElement(By.xpath(".//*[@id='m-main']/li/a[contains(text(),'MP3')]")).isDisplayed());

    }
    @Test
    public void testCityChoose() throws InterruptedException {
        Assert.assertTrue("Section 'Город' is not shown",
                driver.findElement(By.xpath(".//*[@id='city-chooser']")).isDisplayed());
        driver.findElement(By.xpath(".//*[@id='city-chooser']")).click();
        Assert.assertTrue("city 'Харьков' is not shown",
                driver.findElement(By.xpath(".//*[@class='header-city-i']/*[contains(text(),'Харьков')]")).isDisplayed());
        Assert.assertTrue("city 'Киев' is not shown",
                driver.findElement(By.xpath(".//*[@class='header-city-i']/*[contains(text(),'Киев')]")).isDisplayed());
        Assert.assertTrue("city 'Одесса' is not shown",
                driver.findElement(By.xpath(".//*[@class='header-city-i']/*[contains(text(),'Одесса')]")).isDisplayed());
    }
    @Test
    public void testCityEmptyRecycleBin() throws InterruptedException {
        driver.findElement(By.xpath(".//*[@class='sprite-side novisited hub-i-link hub-i-cart-link']")).click();
        Assert.assertTrue("Message'Корзина пуста' is not shown",
                driver.findElement(By.xpath(".//*[@class='empty-cart-title inline sprite-side']")).isDisplayed());
    }
}