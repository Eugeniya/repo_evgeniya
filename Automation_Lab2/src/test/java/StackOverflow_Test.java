import com.sun.org.apache.xpath.internal.operations.Number;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import java.io.File;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.assertTrue;

public class StackOverflow_Test {
    private WebDriver driver;
    private String baseUrl;

    @BeforeClass
    public static void beforeClassSetUp() throws Exception{

    }

    @Before
    public void beforeSetUp() throws Exception{
        driver = new FirefoxDriver();
        baseUrl = "http://stackoverflow.com/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(baseUrl);
    }

    @After
    public void afterSetUp() throws Exception {
        driver.quit();
    }

    @AfterClass
    public static void afterClass() throws Exception{
    }

    @Test
    public void checkfeaturedNumber() throws Exception {
        String number=driver.findElement(By.xpath(".//*[@class='bounty-indicator-tab']")).getText();
        int count=Integer.parseInt(number);
        assertTrue("Count is not > 300", count>300);
    }

    @Test
    public void checkSignUp() throws InterruptedException {
        driver.get(baseUrl);
        driver.findElement(By.xpath("//*[@class='topbar-menu-links']/a[contains (text(), 'sign up')]")).click();
        Assert.assertTrue("Google button is missing",
                driver.findElement(By.xpath(".//*[@class='text']/span[contains(text(),'Google')]")).isDisplayed());
        Assert.assertTrue("Facebook button is missing",
                driver.findElement(By.xpath(".//*[@class='text']/span[contains(text(),'Facebook')]")).isDisplayed());
    }

    @Test
    public void checkQuestion() throws Exception {
        int questionIndex= (int)(Math.random()*10)+1;
        String xPathForQuestion=".//div[\"+questionIndex+\"]//a[@class='question-hyperlink']";
        driver.findElement(By.xpath(xPathForQuestion)).click();
        String xPathForDate=".//*[@id='qinfo']//b[text()='today']";
        assertTrue("Question was not asked today", driver.findElement(By.xpath(xPathForDate)).isDisplayed());
    }
}