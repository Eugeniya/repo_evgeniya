import java.io.BufferedReader;
import java.io.InputStreamReader;
 
public class Lab2_ex4 {
 
    public static void main (String[] args) throws Exception {
   
       BufferedReader reader =  new BufferedReader(new InputStreamReader(System.in));
       
        System.out.println("Please enter first number and than press Enter key: ");
        String a1 = reader.readLine();
        System.out.println("Please enter mathematical operation and than press Enter key: ");
        String operation=reader.readLine();
        System.out.println("Please enter second number and than press Enter key: ");
        String b1 = reader.readLine();

        float a=Float.parseFloat(a1);
        char oper=operation.charAt(0);
        float b=Float.parseFloat(b1);
        
      if (oper=='+')
      {
      	System.out.println("Result= "+(a+b));
      }
      else if (oper=='-')
      {
      	System.out.println("Result= "+(a-b));
      }
      else if (oper=='*')
      {
      	System.out.println("Result= "+(a*b));
      }
      else if (oper=='/')
      {
      	System.out.println("Result= "+(a/b));
      }
      else {
      System.out.println("Incorrect mathematical operation");
      }
         
    }
 }
